#coding:utf-8

#要监控的y2b频道
youtubeList = {
'UCWCc8tO-uUl_7SJXIKJACMw' : 'MEA🍥', #mea
'UCQ0UDLQCjY0rmuxCDE38FGg' : '夏色祭🏮', #祭
'UC1opHUrw8rvnsadT-iGp7Cg' : '湊-阿库娅⚓', #aqua
'UCrhx4PaF3uIo9mDcTxHnmIg' : 'paryi🐇', #paryi
'UChN7P9OhRltW3w9IesC92PA' : '森永みう🍫', #miu
'UC8NZiqKx6fsDT3AVcMiVFyA' : '犬山💙', #犬山
'UCH0ObmokE-zUOeihkKwWySA' : '夢乃栞-Yumeno_Shiori🍄', #大姐
'UCn14Z641OthNps7vppBvZFA' : '千草はな🌼', #hana
'UC0g1AE0DOjBYnLhkgoRWN1w' : '本间向日葵🌻', #葵
'UCL9dLCVvHyMiqjp2RDgowqQ' : '高槻律🚺', #律
'UCkPIfBOLoO0hVPG-tI2YeGg' : '兔鞠mari🥕', #兔鞠mari*
'UCPf-EnX70UM7jqjKwhDmS8g' : '玛格罗那🐟', #魔王*
'UCZ1WJDkMNiZ_QwHnNrVf7Pw' : '饼叽🐥', #饼叽*
'UCzAxQCoeJrmYkHr0cHfD0Nw' : 'yua🔯', #yua*
'UCGcD5iUDG8xiywZeeDxye-A' : '织田信姬🍡', #织田信
'UCXTpFs_3PqI41qX2d9tL2Rw' : '紫咲诗音🌙', #诗音
'UCt0clH12Xk1-Ej5PXKGfdPA' : '♥️♠️物述有栖♦️♣️', #♥️♠️物述有栖♦️♣️
}

#要监控的b站频道
bilibiliList = {
14917277 : '湊-阿库娅⚓', #夸哥
14052636 : '夢乃栞-Yumeno_Shiori🍄', #大姐
12235923 : 'MEA🍥', #吊人
4895312 : 'paryi🐇', #帕里
7962050 : '森永みう🍫', #森永
13946381 : '夏色祭🏮', #祭
10545 : 'A姐💽', #adogsama
12770821 : '千草はな🌼', #hana
#3822389 : '有栖マナ🐾', #mana
4634167 : '犬山💙', #犬山
43067 : 'HAN佬🦊', #han佬
21302477 : '本间向日葵🌻', #葵
947447 : '高槻律🚺', #律
3657657 : '饼叽🐥', #饼叽
7811723 : '小狐狸Soy🌸',#soy
21132965 : '紫咲诗音🌙', #紫咲诗音
21449083 : '♥️♠️物述有栖♦️♣️',#♥️♠️物述有栖♦️♣️
}

#要监控的twitcasting频道
twitcastingList = {
'kaguramea_vov' : 'MEA🍥', #吊人
'morinaga_miu' : '森永miu🍫', #miu
'norioo_' : '海苔男🍡', #海苔男
'natsuiromatsuri' : '夏色祭🏮', #夏色祭
'c:yumeno_shiori' : 'shiori大姐🍄', #p家大姐
'maturin_love221' : '爱小姐☂︎', #test
're2_takatsuki' : '高槻律🚺', #律
'merrysan_cas_' : '球王🏀', #球王
}

#要监控的fc2频道
fc2List = {
'78847652' : "shiori🍄",#大姐
}
